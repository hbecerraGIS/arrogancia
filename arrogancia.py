geom_to_wkt(@map_extent)
geom_to_wkt(bounds($geometry))
area($geometry)
area(@map_extent)
area(intersection($geometry,@map_extent))
round(sum(area(intersection($geometry,@map_extent))),2) || 'm²'

'Banquetas '||'\n'||round(sum(area(intersection($geometry,@map_extent))),2)||'m²'
'Vialidades '||'\n'||round(sum(area(intersection($geometry,@map_extent))),2)||'m²'
BUFFER(COLLECT($geometry),0)

--MapTips

<H1>[% 'Ancho '|| '\n' ||round("ancho",2)||'m²'  %] </H1>